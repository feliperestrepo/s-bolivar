import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BolivarAppComponent } from './bolivar-app.component';

describe('BolivarAppComponent', () => {
  let component: BolivarAppComponent;
  let fixture: ComponentFixture<BolivarAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BolivarAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BolivarAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
