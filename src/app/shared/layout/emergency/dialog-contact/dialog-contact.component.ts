import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { DialogConfirmationComponent } from '../dialog-confirmation/dialog-confirmation.component';

@Component({
  selector: 'app-dialog-contact',
  templateUrl: './dialog-contact.component.html',
  styleUrls: ['./dialog-contact.component.scss']
})
export class DialogContactComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialogConfirmation() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('respuesta dialog confirmation');
    });
  }

  ngOnInit() {
  }

}
