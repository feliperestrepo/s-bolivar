import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { DialogContactComponent } from './dialog-contact/dialog-contact.component';

@Component({
  selector: 'app-emergency',
  templateUrl: './emergency.component.html',
  styleUrls: ['./emergency.component.scss']
})
export class EmergencyComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialog() {
    const dialogRef = this.dialog.open(DialogContactComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('respuesta dialog');
    });
  }

  ngOnInit() {
  }

}
