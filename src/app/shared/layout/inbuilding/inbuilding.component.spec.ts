import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InbuildingComponent } from './inbuilding.component';

describe('InbuildingComponent', () => {
  let component: InbuildingComponent;
  let fixture: ComponentFixture<InbuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InbuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InbuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
