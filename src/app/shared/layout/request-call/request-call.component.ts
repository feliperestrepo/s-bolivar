import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { DialogCallComponent } from './dialog-call/dialog-call.component';

@Component({
  selector: 'app-request-call',
  templateUrl: './request-call.component.html',
  styleUrls: ['./request-call.component.scss']
})
export class RequestCallComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialog() {
    const dialogRef = this.dialog.open(DialogCallComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('respuesta dialog');
    });
  }

  ngOnInit() {
  }

}
