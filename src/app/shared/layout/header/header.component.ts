import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public activeLink:number;

  constructor(private router: Router) { }

  ngOnInit() {
    switch (this.router.url) {
			case "/":
				this.activeLink = 0;
      break;
      case "/productos":
				this.activeLink = 0;
			break;
			case "/empresas/sub-categoria":
				this.activeLink = 1;
      break;
      case "/empresas/otros":
				this.activeLink = 1;
      break;
      case "/arl":
				this.activeLink = 2;
      break;
      case "/davivienda":
				this.activeLink = 3;
			break;
		}
  }

}
