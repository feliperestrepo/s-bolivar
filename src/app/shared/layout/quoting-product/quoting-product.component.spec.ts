import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotingProductComponent } from './quoting-product.component';

describe('QuotingProductComponent', () => {
  let component: QuotingProductComponent;
  let fixture: ComponentFixture<QuotingProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotingProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotingProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
