import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleComponent } from './people/people.component';
import { ProductsComponent } from './products/products.component';
import { OthersComponent } from './companies/others/others.component';
import { SubCategoryComponent } from './companies/sub-category/sub-category.component';
import { TemplateComponent } from './static-pages/template/template.component';

const routes: Routes = [
  {
    path: '',
    data: { breadcrumd: 'Inicio' },
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            component: PeopleComponent
          },
          {
            path: 'productos',
            component: ProductsComponent
          },
          {
            path: 'paginas-estaticas',
            component: TemplateComponent
          }
        ]
      },
      {
        path: 'empresas',
        children: [
          {
            path: '',
            component: PeopleComponent
          },
          {
            path: 'otros',
            component: OthersComponent
          },
          {
            path: 'sub-categoria',
            component: SubCategoryComponent
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
