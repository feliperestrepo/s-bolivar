
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NguCarouselModule } from '@ngu/carousel';

// Material
import { ComponentsMaterial } from './components-material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// components
import { HeaderComponent } from './shared/layout/header/header.component';
import { PeopleComponent } from './people/people.component';
import { NavMobileComponent } from './shared/layout/nav-mobile/nav-mobile.component';
import { QuotingComponent } from './shared/layout/quoting/quoting.component';
import { BannerHeroComponent } from './shared/layout/banner-hero/banner-hero.component';
import { EmergencyComponent } from './shared/layout/emergency/emergency.component';
import { InsuranceComponent } from './shared/layout/insurance/insurance.component';
import { ServicesComponent } from './shared/layout/services/services.component';
import { BolivarAppComponent } from './shared/layout/bolivar-app/bolivar-app.component';
import { BlogComponent } from './shared/layout/blog/blog.component';
import { RequestCallComponent } from './shared/layout/request-call/request-call.component';
import { FaqComponent } from './shared/layout/faq/faq.component';
import { InbuildingComponent } from './shared/layout/inbuilding/inbuilding.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { ProductsComponent } from './products/products.component';
import { BannerProductsComponent } from './shared/layout/banner-products/banner-products.component';
import { QuotingProductComponent } from './shared/layout/quoting-product/quoting-product.component';
import { OthersComponent } from './companies/others/others.component';
import { SubCategoryComponent } from './companies/sub-category/sub-category.component';
import { TemplateComponent } from './static-pages/template/template.component';
import { BreadcrumbComponent } from './shared/layout/breadcrumb/breadcrumb.component';
import { DirectoryComponent } from './shared/layout/directory/directory.component';
import { DocumentsComponent } from './shared/layout/documents/documents.component';
import { PaginatorComponent } from './shared/layout/paginator/paginator.component';
import { CardsComponent } from './shared/layout/cards/cards.component';
import { QuoteComponent } from './shared/layout/quote/quote.component';
import { MultimediaComponent } from './shared/layout/multimedia/multimedia.component';
import { TextPhotoComponent } from './shared/layout/text-photo/text-photo.component';
import { TextListComponent } from './shared/layout/text-list/text-list.component';
import { ImagesComponent } from './shared/layout/images/images.component';
import { CustomerServiceComponent } from './shared/layout/customer-service/customer-service.component';
import { DialogContactComponent } from './shared/layout/emergency/dialog-contact/dialog-contact.component';
import { DialogCallComponent } from './shared/layout/request-call/dialog-call/dialog-call.component';
import { DialogConfirmationComponent } from './shared/layout/emergency/dialog-confirmation/dialog-confirmation.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PeopleComponent,
    NavMobileComponent,
    QuotingComponent,
    BannerHeroComponent,
    EmergencyComponent,
    InsuranceComponent,
    ServicesComponent,
    BolivarAppComponent,
    BlogComponent,
    RequestCallComponent,
    FaqComponent,
    InbuildingComponent,
    FooterComponent,
    ProductsComponent,
    BannerProductsComponent,
    QuotingProductComponent,
    OthersComponent,
    SubCategoryComponent,
    TemplateComponent,
    BreadcrumbComponent,
    DirectoryComponent,
    DocumentsComponent,
    PaginatorComponent,
    CardsComponent,
    QuoteComponent,
    MultimediaComponent,
    TextPhotoComponent,
    TextListComponent,
    ImagesComponent,
    CustomerServiceComponent,
    DialogContactComponent,
    DialogCallComponent,
    DialogConfirmationComponent
  ],
  entryComponents: [
    DialogContactComponent,
    DialogCallComponent,
    DialogConfirmationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NguCarouselModule,
    ComponentsMaterial
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
